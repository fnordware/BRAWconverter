# BRAWconverter

Utility for converting Blackmagic RAW files to image sequences

# Author

[Brendan Bolles](http://github.com/fnordware/)

# License

[GPL v2](https://www.gnu.org/licenses/)
