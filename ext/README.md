ext
===

This directory holds git submodules that point to libraries needed by BRAWconverter.

You will need to manually add the Blackmagic RAW SDK, found somewhere on the [Blackmagic site](https://www.blackmagicdesign.com/) as part of the Blackmagic RAW package.

If the submodule contents are missing, you should be able to get them by typing:

`git submodule init`
`git submodule update`
