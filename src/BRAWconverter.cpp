/* ---------------------------------------------------------------------
//
// BRAWconverter - Utility for converting Blackmagic RAW files
// Copyright (c) 2020,  Brendan Bolles, http://www.fnordware.com
//
// This file is part of BRAWconverter.
//
// BRAWconverter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// -------------------------------------------------------------------*/

#include "BlackmagicRawAPI.h"

#include "MetalContext.h"

#include <CoreServices/CoreServices.h>
#include <ImageIO/ImageIO.h>
#include <mach/mach.h>

#include <ImfOutputFile.h>
#include <ImfChannelList.h>
#include <IlmThread.h>
#include <IlmThreadPool.h>
#include <ImfStandardAttributes.h>
#include <ImfCompressionAttribute.h>
#include <ImfIntAttribute.h>

#include "DPX.h"


class Exc : public std::exception
{
  public:
	Exc(const std::string &what) noexcept : _what(what) {}
	virtual ~Exc() noexcept {}
	virtual const char* what() const noexcept { return _what.c_str(); }

  private:
	const std::string _what;
};


class Err : public std::exception
{
  public:
	Err(HRESULT err = S_OK) noexcept : _err(err) {}
	virtual ~Err() noexcept {}
	virtual Err & operator = (HRESULT err);
	virtual HRESULT err() const noexcept { return _err; }
	virtual const char* what() const noexcept { return "Got error"; }

  private:
	HRESULT _err;
};


Err &
Err::operator = (HRESULT err)
{
	_err = err;
	
	if(_err != S_OK)
		throw *this;
	
	return *this;
}


class CopyHalfTask : public IlmThread::Task
{
  public:
	CopyHalfTask(IlmThread::TaskGroup *group, char *half_buffer, size_t half_rowbytes, char *float_buffer, size_t float_rowbytes, int width, int row);
	virtual ~CopyHalfTask() {}
	
	virtual void execute();

  private:
	char *_half_buffer;
	size_t _half_rowbytes;
	char *_float_buffer;
	size_t _float_rowbytes;
	int _width;
	int _row;
};


CopyHalfTask::CopyHalfTask(IlmThread::TaskGroup *group, char *half_buffer, size_t half_rowbytes, char *float_buffer, size_t float_rowbytes, int width, int row) :
	IlmThread::Task(group),
	_half_buffer(half_buffer),
	_half_rowbytes(half_rowbytes),
	_float_buffer(float_buffer),
	_float_rowbytes(float_rowbytes),
	_width(width),
	_row(row)
{

}


void
CopyHalfTask::execute()
{
	half *h = (half *)(_half_buffer + (_half_rowbytes * _row));
	float *f = (float *)(_float_buffer + (_float_rowbytes * _row));
	
	for(int i=0; i < _width; i++)
	{
		*h++ = *f++;
	}
}


class RemoveAlphaTask : public IlmThread::Task
{
  public:
	RemoveAlphaTask(IlmThread::TaskGroup *group, uint8_t *rgba_row, int width);
	virtual ~RemoveAlphaTask() {}
	
	virtual void execute();

  private:
	uint8_t *_rgba_row;
	const int _width;
};


RemoveAlphaTask::RemoveAlphaTask(IlmThread::TaskGroup *group, uint8_t *rgba_row, int width) :
	IlmThread::Task(group),
	_rgba_row(rgba_row),
	_width(width)
{

}


void
RemoveAlphaTask::execute()
{
	uint8_t *reader = _rgba_row;
	uint8_t *writer = _rgba_row;
	
	for(int x=0; x < _width; x++)
	{
		*writer++ = *reader++;
		*writer++ = *reader++;
		*writer++ = *reader++;
		reader++;
	}
}

#pragma mark-

static std::string
StringRefToString(CFStringRef ref)
{
	const char *refStr = CFStringGetCStringPtr(ref, kCFStringEncodingUTF8);
	
	if(refStr != nullptr)
	{
		return refStr;
	}
	else
	{
		char refStrBuf[2048];
		const Boolean gotString = CFStringGetCString(ref, refStrBuf, 2048, kCFStringEncodingUTF8);
		
		return (gotString ? refStrBuf : "????");
	}
}

#pragma mark-

template<typename T>
static T
GetVariant(const Variant &v);

template<>
CFStringRef
GetVariant<CFStringRef>(const Variant &v)
{
	assert(v.vt == blackmagicRawVariantTypeString);
	return v.bstrVal;
}

template<>
float
GetVariant<float>(const Variant &v)
{
	assert(v.vt == blackmagicRawVariantTypeFloat32);
	return v.fltVal;
}

template<>
int16_t
GetVariant<int16_t>(const Variant &v)
{
	assert(v.vt == blackmagicRawVariantTypeS16);
	return v.iVal;
}

template<>
uint16_t
GetVariant<uint16_t>(const Variant &v)
{
	assert(v.vt == blackmagicRawVariantTypeU16);
	return v.uiVal;
}

template<>
uint32_t
GetVariant<uint32_t>(const Variant &v)
{
	assert(v.vt == blackmagicRawVariantTypeU32);
	return v.uintVal;
}


template<typename T>
static void
SetVariant(Variant &v, const std::string &val);

template<>
void
SetVariant<CFStringRef>(Variant &v, const std::string &val)
{
	v.vt = blackmagicRawVariantTypeString;
	v.bstrVal = CFStringCreateWithCString(kCFAllocatorDefault, val.c_str(), kCFStringEncodingUTF8);
}

template<>
void
SetVariant<float>(Variant &v, const std::string &val)
{
	v.vt = blackmagicRawVariantTypeFloat32;
	v.fltVal = std::stof(val);
}

template<>
void
SetVariant<int16_t>(Variant &v, const std::string &val)
{
	v.vt = blackmagicRawVariantTypeS16;
	v.iVal = std::stoi(val);
}

template<>
void
SetVariant<uint16_t>(Variant &v, const std::string &val)
{
	v.vt = blackmagicRawVariantTypeU16;
	v.uiVal = std::stoi(val);
}

template<>
void
SetVariant<uint32_t>(Variant &v, const std::string &val)
{
	v.vt = blackmagicRawVariantTypeU32;
	v.uintVal = std::stoi(val);
}


template<typename T>
static void
CheckClipAttr(BlackmagicRawClipProcessingAttribute attribute, const std::string &arg, const Variant &v, IBlackmagicRawConstants *constants, CFStringRef cameraType);

template<>
void
CheckClipAttr<CFStringRef>(BlackmagicRawClipProcessingAttribute attribute, const std::string &arg, const Variant &v, IBlackmagicRawConstants *constants, CFStringRef cameraType)
{
	Variant *varArray = nullptr;
	uint32_t count = 0;
	bool readOnly = true;
	
	Err err = constants->GetClipProcessingAttributeList(cameraType, attribute, nullptr, &count, &readOnly);
	
	if(count > 0)
	{
		const std::string val = StringRefToString(GetVariant<CFStringRef>(v));
		
		varArray = new Variant[count];
		
		err = constants->GetClipProcessingAttributeList(cameraType, attribute, varArray, &count, &readOnly);
		
		bool foundMatch = false;
		
		for(int i=0; i < count && !foundMatch; i++)
		{
			assert(varArray[i].vt == blackmagicRawVariantTypeString);
		
			const std::string option = StringRefToString(GetVariant<CFStringRef>(varArray[i]));
			
			if(option == val)
				foundMatch = true;
		}
		
		if(!foundMatch)
		{
			std::string errMsg = "Invalid choice for " + arg + ", options are:\n";
			
			for(int i=0; i < count && !foundMatch; i++)
			{
				const std::string option = StringRefToString(GetVariant<CFStringRef>(varArray[i]));
				
				errMsg += option + "\n";
			}
			
			delete [] varArray;
			
			throw Exc(errMsg);
		}
		
		delete [] varArray;
	}
	else
		assert(false);
}

template<>
void
CheckClipAttr<uint16_t>(BlackmagicRawClipProcessingAttribute attribute, const std::string &arg, const Variant &v, IBlackmagicRawConstants *constants, CFStringRef cameraType)
{
	Variant valueMin, valueMax;
	bool readOnly = true;

	Err err = constants->GetClipProcessingAttributeRange(cameraType, attribute, &valueMin, &valueMax, &readOnly);
	
	assert(!readOnly);
	assert(v.vt == valueMin.vt);
	assert(v.vt == valueMax.vt);
	
	if(GetVariant<uint16_t>(v) < GetVariant<uint16_t>(valueMin) || GetVariant<uint16_t>(v) > GetVariant<uint16_t>(valueMax))
	{
		std::stringstream ss;
		
		ss << "Invalid choice for " << arg << ", range is " << GetVariant<uint16_t>(valueMin) << " to " << GetVariant<uint16_t>(valueMax) << std::endl;
		
		throw Exc(ss.str());
	}
}


template<>
void
CheckClipAttr<float>(BlackmagicRawClipProcessingAttribute attribute, const std::string &arg, const Variant &v, IBlackmagicRawConstants *constants, CFStringRef cameraType)
{
	Variant valueMin, valueMax;
	bool readOnly = true;

	Err err = constants->GetClipProcessingAttributeRange(cameraType, attribute, &valueMin, &valueMax, &readOnly);
	
	assert(!readOnly);
	assert(v.vt == valueMin.vt);
	assert(v.vt == valueMax.vt);
	
	if(GetVariant<float>(v) < GetVariant<float>(valueMin) || GetVariant<float>(v) > GetVariant<float>(valueMax))
	{
		std::stringstream ss;
		
		ss << "Invalid choice for " << arg << ", range is " << GetVariant<float>(valueMin) << " to " << GetVariant<float>(valueMax) << std::endl;
		
		throw Exc(ss.str());
	}
}


template<typename T>
static void
SetClipAttr(IBlackmagicRawClipProcessingAttributes *clipAttrs, BlackmagicRawClipProcessingAttribute attribute, const std::string &arg, const std::string &val, IBlackmagicRawConstants *constants, CFStringRef cameraType)
{
	Variant v;
	SetVariant<T>(v, val);
	
	CheckClipAttr<T>(attribute, arg, v, constants, cameraType);
	
	Err err = clipAttrs->SetClipAttribute(attribute, &v);
}


template<typename T>
static void
CheckFrameAttr(BlackmagicRawFrameProcessingAttribute attribute, const std::string &arg, const std::string &val, IBlackmagicRawConstants *constants, CFStringRef cameraType)
{
	Variant value;
	SetVariant<T>(value, val);
	
	Variant valueMin, valueMax;
	bool readOnly = true;
	
	Err err = constants->GetFrameProcessingAttributeRange(cameraType, attribute, &valueMin, &valueMax, &readOnly);
	
	assert(!readOnly);
	assert(value.vt == valueMin.vt);
	assert(value.vt == valueMax.vt);
	
	if(GetVariant<T>(value) < GetVariant<T>(valueMin) || GetVariant<T>(value) > GetVariant<T>(valueMax))
	{
		std::stringstream ss;
		
		ss << "Invalid choice for " << arg << ", range is " << GetVariant<T>(valueMin) << " to " << GetVariant<T>(valueMax) << std::endl;
		
		throw Exc(ss.str());
	}
}


template<typename T>
static void
SetVariantVal(Variant &v, const T &val);

template<>
void
SetVariantVal<float>(Variant &v, const float &val)
{
	v.vt = blackmagicRawVariantTypeFloat32;
	v.fltVal = val;
}

template<>
void
SetVariantVal<int16_t>(Variant &v, const int16_t &val)
{
	v.vt = blackmagicRawVariantTypeS16;
	v.iVal = val;
}

template<>
void
SetVariantVal<uint32_t>(Variant &v, const uint32_t &val)
{
	v.vt = blackmagicRawVariantTypeU32;
	v.uintVal = val;
}


template<typename T>
static void
SetFrameAttr(IBlackmagicRawFrameProcessingAttributes *frameAttrs, BlackmagicRawFrameProcessingAttribute attribute, const T &val)
{
	Variant v;
	SetVariantVal<T>(v, val);
	
	Err err = frameAttrs->SetFrameAttribute(attribute, &v);
}


static std::string
GetVariantAttr(const Variant &val, Imf::Attribute **attribute)
{
	std::stringstream ss;
	Imf::Attribute *attr = nullptr;

	if(val.vt == blackmagicRawVariantTypeU8)
	{
		ss << val.uiVal;
		
		attr = new Imf::IntAttribute(val.uiVal);
	}
	else if(val.vt == blackmagicRawVariantTypeS16)
	{
		ss << val.iVal;
		
		attr = new Imf::IntAttribute(val.iVal);
	}
	else if(val.vt == blackmagicRawVariantTypeU16)
	{
		ss << val.uiVal;
		
		attr = new Imf::IntAttribute(val.uiVal);
	}
	else if(val.vt == blackmagicRawVariantTypeS32)
	{
		ss << val.intVal;
		
		attr = new Imf::IntAttribute(val.intVal);
	}
	else if(val.vt == blackmagicRawVariantTypeU32)
	{
		ss << val.uintVal;
		
		attr = new Imf::IntAttribute(val.uintVal);
	}
	else if(val.vt == blackmagicRawVariantTypeFloat32)
	{
		ss << val.fltVal;
		
		attr = new Imf::FloatAttribute(val.fltVal);
	}
	else if(val.vt == blackmagicRawVariantTypeString)
	{
		const std::string s = StringRefToString(val.bstrVal);
		
		ss << s;
		
		attr = new Imf::StringAttribute(s);
	}
	else if(val.vt == blackmagicRawVariantTypeSafeArray)
	{
		const SafeArray *arr = val.parray;
		
		assert(CFDataGetLength(arr->data) == 0); // not sure why
		
		if((arr->bounds.cElements * arr->cDims) <= 64)
		{
			std::stringstream altss;
			
			const UInt8 *data = CFDataGetMutableBytePtr(arr->data);
			
			assert(arr->bounds.lLbound == 0);
			
			for(int i=0; i < arr->bounds.cElements; i++)
			{
				for(int j=0; j < arr->cDims; j++)
				{
					if(j > 0 || i > 0)
						altss << " ";
					
					if(arr->variantType == blackmagicRawVariantTypeU8)
					{
						altss << *data++;
					}
					else if(arr->variantType == blackmagicRawVariantTypeS16)
					{
						int16_t *d = (int16_t *)data;
						altss << *d;
						data += sizeof(int16_t);
					}
					else if(arr->variantType == blackmagicRawVariantTypeU16)
					{
						uint16_t *d = (uint16_t *)data;
						altss << *d;
						data += sizeof(uint16_t);
					}
					else if(arr->variantType == blackmagicRawVariantTypeS32)
					{
						int32_t *d = (int32_t *)data;
						altss << *d;
						data += sizeof(int32_t);
					}
					else if(arr->variantType == blackmagicRawVariantTypeU32)
					{
						uint32_t *d = (uint32_t *)data;
						altss << *d;
						data += sizeof(uint32_t);
					}
					else if(arr->variantType == blackmagicRawVariantTypeFloat32)
					{
						float *d = (float *)data;
						altss << *d;
						data += sizeof(float);
					}
					else
						altss << "?";
				}
			}
			
			ss << altss.str();
			
			attr = new Imf::StringAttribute(altss.str());
		}
		else
		{
			const char *dataType = (arr->variantType == blackmagicRawVariantTypeU8 ? "uint8" :
									arr->variantType == blackmagicRawVariantTypeS16 ? "int16" :
									arr->variantType == blackmagicRawVariantTypeU16 ? "uint16" :
									arr->variantType == blackmagicRawVariantTypeS32 ? "int32" :
									arr->variantType == blackmagicRawVariantTypeU32 ? "uint32" :
									arr->variantType == blackmagicRawVariantTypeFloat32 ? "float" :
									"???");

			ss << "[" << arr->bounds.cElements << " x " <<  arr->cDims << " x " << dataType << "]";
		}
	}
	else
	{
		ss << "Unknown type: " << val.vt;
	}
	
	if(attribute != nullptr)
		*attribute = attr;
	
	return ss.str();
}

#pragma mark-

static void
GetClipInfo(IBlackmagicRawClip* clip, Imf::Header::AttributeMap &attrMap)
{
	try
	{
		Err err;
		
		uint32_t width = 0, height = 0;
		float framerate = 0.f;
		uint64_t frameCount = 0;
	
		err = clip->GetWidth(&width);
		err = clip->GetHeight(&height);
		err = clip->GetFrameRate(&framerate);
		err = clip->GetFrameCount(&frameCount);
		
		std::cout << "Dimensions: " << width << " x " << height << std::endl;
		std::cout << "Framerate: " << framerate << std::endl;
		std::cout << "Frames: " << frameCount << std::endl;
		
		attrMap["braw/clip/framerate"] = new Imf::FloatAttribute(framerate);
		
		float fps_inte;
		const float fps_frac = modff(framerate, &fps_inte);
		
		if(fps_frac == 0.f)
		{
			attrMap["framesPerSecond"] = new Imf::RationalAttribute(Imf::Rational(fps_inte, 1));
		}
		else
		{
			const int d = 1001;
			const int n = (framerate * (float)d) + 0.5f;
			
			attrMap["framesPerSecond"] = new Imf::RationalAttribute(Imf::Rational(n, d));
		}
		
		
		IBlackmagicRawMetadataIterator *metadataIterator = nullptr;
		err = clip->GetMetadataIterator(&metadataIterator);
		
		do{
			CFStringRef keyRef = nullptr;
			metadataIterator->GetKey(&keyRef);
			
			const std::string key = StringRefToString(keyRef);
			
			Variant data;
			err = metadataIterator->GetData(&data);
			
			Imf::Attribute *attr = nullptr;
			
			const std::string valStr = GetVariantAttr(data, &attr);
			
			std::cout << key << ": " << valStr << std::endl;
			
			if(attr != nullptr)
			{
				const std::string attrKey = "braw/clip/" + key;
				
				attrMap[attrKey.c_str()] = attr;
			}
			
		}while(metadataIterator->Next() == S_OK);
		
		
		//CFStringRef cameraTypeRef = nullptr;
		//uint32_t multicardFileCount = 0;
		bool sidecarFileAttached = false;
		
		//err = clip->GetCameraType(&cameraTypeRef);
		//err = clip->GetMulticardFileCount(&multicardFileCount);
		err = clip->GetSidecarFileAttached(&sidecarFileAttached);
		
		//std::cout << "Camera Type: " << StringRefToString(cameraTypeRef) << std::endl;
		//std::cout << "Multicard File Count: " << multicardFileCount << std::endl;
		std::cout << "Sidecar file attached: " << (sidecarFileAttached ? "YES" : "NO") << std::endl;
		
		std::cout << std::endl;
	}
	catch(const Exc &e)
	{
		std::cerr << "Command error: " << e.what() << std::endl;
	}
	catch(const Err &e)
	{
		std::cerr << "Failed with error code " << e.err() << std::endl;
	}
	catch(...)
	{
		std::cerr << "Unknown exception!" << std::endl;
	}
}


static void
GetClipProcessingInfo(IBlackmagicRawClipProcessingAttributes* clipAttrs, Imf::Header::AttributeMap &attrMap)
{
	const BlackmagicRawClipProcessingAttribute attrs[] = {
		blackmagicRawClipProcessingAttributeColorScienceGen,
		blackmagicRawClipProcessingAttributeGamma,
		blackmagicRawClipProcessingAttributeGamut,
		blackmagicRawClipProcessingAttributeToneCurveContrast,
		blackmagicRawClipProcessingAttributeToneCurveSaturation,
		blackmagicRawClipProcessingAttributeToneCurveMidpoint,
		blackmagicRawClipProcessingAttributeToneCurveHighlights,
		blackmagicRawClipProcessingAttributeToneCurveShadows,
		blackmagicRawClipProcessingAttributeToneCurveVideoBlackLevel,
		blackmagicRawClipProcessingAttributeToneCurveBlackLevel,
		blackmagicRawClipProcessingAttributeToneCurveWhiteLevel,
		blackmagicRawClipProcessingAttributeHighlightRecovery,
		blackmagicRawClipProcessingAttributeAnalogGainIsConstant,
		blackmagicRawClipProcessingAttributeAnalogGain,
		blackmagicRawClipProcessingAttributePost3DLUTMode,
		blackmagicRawClipProcessingAttributeEmbeddedPost3DLUTName,
		blackmagicRawClipProcessingAttributeEmbeddedPost3DLUTTitle,
		blackmagicRawClipProcessingAttributeEmbeddedPost3DLUTSize,
		blackmagicRawClipProcessingAttributeEmbeddedPost3DLUTData,
		blackmagicRawClipProcessingAttributeSidecarPost3DLUTName,
		blackmagicRawClipProcessingAttributeSidecarPost3DLUTTitle,
		blackmagicRawClipProcessingAttributeSidecarPost3DLUTSize,
		blackmagicRawClipProcessingAttributeSidecarPost3DLUTData };
	
	const char * const attrNames[] = {
		"color_science_gen",
		"gamma",
		"gamut",
		"contrast",
		"saturation",
		"midpoint",
		"highlights",
		"shadows",
		"video_black_level",
		"black_level",
		"white_level",
		"highlight_recovery",
		"analog_gain_is_constant",
		"analog_gain",
		"lut_mode",
		"lut_name",
		"lut_title",
		"lut_size",
		"lut_data",
		"sidecar_lut_name",
		"sidecar_lut_title",
		"sidecar_lut_size",
		"sidecar_lut_data" };
	
	try
	{
		Err err;
		
		Variant gamma;
		err = clipAttrs->GetClipAttribute(blackmagicRawClipProcessingAttributeGamma, &gamma);
		
		assert(gamma.vt == blackmagicRawVariantTypeString);
		
		const std::string gammaStr = StringRefToString(gamma.bstrVal);
		
		const bool printGamma = (gammaStr == "Blackmagic Design Film" ||
									gammaStr == "Blackmagic Design Extended Video" ||
									gammaStr == "Blackmagic Design Custom");
		
		const int numAttrs = (printGamma ? (sizeof(attrs) / sizeof(BlackmagicRawClipProcessingAttribute)) : 3);

		for(int i=0; i < numAttrs; i++)
		{
			const std::string key = attrNames[i];
			
			Variant data;
			HRESULT err2 = clipAttrs->GetClipAttribute(attrs[i], &data);
			
			if(err2 == S_OK)
			{
				Imf::Attribute *attr = nullptr;
			
				const std::string valStr = GetVariantAttr(data, &attr);
				
				if(valStr != "Disabled")
				{
					std::cout << key << ": " << valStr << std::endl;
				
					if(attr != nullptr)
					{
						const std::string attrKey = "braw/clip/color/" + key;
						
						attrMap[attrKey.c_str()] = attr;
					}
				}
			}
		}
		
		std::cout << std::endl;
		
		
		Variant gamut;
		err = clipAttrs->GetClipAttribute(blackmagicRawClipProcessingAttributeGamut, &gamut);
		
		assert(gamut.vt == blackmagicRawVariantTypeString);
		
		const std::string gamutStr = StringRefToString(gamut.bstrVal);
		
		attrMap["chromaticitiesName"] = new Imf::StringAttribute(gamutStr);
		
		if(gamutStr == "Blackmagic Design")
		{
			// Thanks to Thomas Mansencal and Nick Shaw from colour-science.org
			// https://github.com/nick-shaw/colour/blob/8eae9d10d74a0a4967e6fca48b754caa7c203b41/colour/models/rgb/datasets/blackmagic.py
			
			// Which BMD gamut is it???
			
			// BMD Film v1
			//attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
			//									Imf::Chromaticities(
			//										Imath::V2f(0.9173f, 0.2502f),
			//										Imath::V2f(0.2833f, 1.7072f),
			//										Imath::V2f(0.0856f, -0.0708f),
			//										Imath::V2f(0.3135f, 0.3305f) ) );

			// BMD 4K Film v1
			//attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
			//									Imf::Chromaticities(
			//										Imath::V2f(0.7422f, 0.2859f),
			//										Imath::V2f(0.4140f, 1.3035f),
			//										Imath::V2f(0.0342f, -0.0833f),
			//										Imath::V2f(0.3135f, 0.3305f) ) );
			
			// BMD 4K Film v3
			//attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
			//									Imf::Chromaticities(
			//										Imath::V2f(1.0625f, 0.3948f),
			//										Imath::V2f(0.3689f, 0.7775f),
			//										Imath::V2f(0.0956f, -0.0332f),
			//										Imath::V2f(0.3135f, 0.3305f) ) );
			
			// BMD 46K Film v1
			//attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
			//									Imf::Chromaticities(
			//										Imath::V2f(0.9175f, 0.2983f),
			//										Imath::V2f(0.2983f, 1.2835f), // r.y == g.x ??
			//										Imath::V2f(0.0756f, -0.0860f),
			//										Imath::V2f(0.3127f, 0.3290f) ) );
			
			// BMD 46K Film v3
			//attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
			//									Imf::Chromaticities(
			//										Imath::V2f(0.8608f, 0.3689f),
			//										Imath::V2f(0.3282f, 0.6156f),
			//										Imath::V2f(0.0783f, -0.0233f),
			//										Imath::V2f(0.3127f, 0.3290f) ) );
			
			// This one, apparently!
			
			// BMD Wide Gamut v4
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.7177f, 0.3171f),
													Imath::V2f(0.2280f, 0.8616f),
													Imath::V2f(0.1006f, -0.0820f),
													Imath::V2f(0.3127f, 0.3290f) ) );
		}
		else if(gamutStr == "Rec.709")
		{
			// https://en.wikipedia.org/wiki/Rec._709
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.64f, 0.33f),
													Imath::V2f(0.30f, 0.60f),
													Imath::V2f(0.15f, 0.06f),
													Imath::V2f(0.3127f, 0.3290f) ) );
		}
		else if(gamutStr == "Rec.2020")
		{
			// https://en.wikipedia.org/wiki/Rec._2020
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.708f, 0.292f),
													Imath::V2f(0.17f, 0.797f),
													Imath::V2f(0.131f, 0.046f),
													Imath::V2f(0.3127f, 0.3290f) ) );
		}
		else if(gamutStr == "DCI-P3 D65")
		{
			// https://en.wikipedia.org/wiki/DCI-P3
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.680f, 0.320f),
													Imath::V2f(0.265f, 0.690f),
													Imath::V2f(0.150f, 0.060f),
													Imath::V2f(0.3127f, 0.3290f) ) );
		}
		else if(gamutStr == "DCI-P3 Theater")
		{
			// https://en.wikipedia.org/wiki/DCI-P3
			// http://www.hp.com/united-states/campaigns/workstations/pdfs/lp2480zx-dci--p3-emulation.pdf
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.680f, 0.320f),
													Imath::V2f(0.265f, 0.690f),
													Imath::V2f(0.150f, 0.060f),
													Imath::V2f(0.314f, 0.351f) ) );
		}
		else if(gamutStr == "ACES AP0")
		{
			// https://en.wikipedia.org/wiki/Academy_Color_Encoding_System#ACES_Color_Spaces
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.7347f, 0.2653f),
													Imath::V2f(0.0f, 1.f),
													Imath::V2f(0.0001f, -0.0770f),
													Imath::V2f(0.32168f, 0.33767f) ) );
		}
		else if(gamutStr == "ACES AP1")
		{
			// https://en.wikipedia.org/wiki/Academy_Color_Encoding_System#ACES_Color_Spaces
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.713f, 0.293f),
													Imath::V2f(0.165f, 0.830f),
													Imath::V2f(0.128f, 0.044f),
													Imath::V2f(0.32168f, 0.33767f) ) );
		}
		else if(gamutStr == "CIE 1931 XYZ D65")
		{
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.f, 1.f),
													Imath::V2f(1.f, 0.f),
													Imath::V2f(0.f, 0.f),
													Imath::V2f(0.3127f, 0.3290f) ) );
		}
		else if(gamutStr == "CIE 1931 XYZ D50 (PCS)")
		{
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.f, 1.f),
													Imath::V2f(1.f, 0.f),
													Imath::V2f(0.f, 0.f),
													Imath::V2f(0.3457f, 0.3585f) ) );
		}
		else if(gamutStr == "V-Gamut")
		{
			// https://pro-av.panasonic.net/en/cinema_camera_varicam_eva/support/pdf/VARICAM_V-Log_V-Gamut.pdf
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.730f, 0.280),
													Imath::V2f(0.165f, 0.840f),
													Imath::V2f(0.100f, -0.030f),
													Imath::V2f(0.3127f, 0.3290f) ) );
		}
		else if(gamutStr == "Canon Cinema Gamut")
		{
			// https://nick-shaw.github.io/cinematiccolor/common-rgb-color-spaces.html#x34-1450004.1.12
			// https://www.usa.canon.com/internet/portal/us/home/learn/education/topics/article/2018/july/eos-c300-mark-ii-looking-forward-wide-color-spaces/eos-c300-mark-ii-looking-forward-wide-color-spaces/
			attrMap["chromaticities"] = new Imf::ChromaticitiesAttribute(
												Imf::Chromaticities(
													Imath::V2f(0.740f, 0.270),
													Imath::V2f(0.170f, 1.140f),
													Imath::V2f(0.080f, -0.100f),
													Imath::V2f(0.3127f, 0.3290f) ) );
		}
		else
			assert(false);
	}
	catch(const Exc &e)
	{
		std::cerr << "Command error: " << e.what() << std::endl;
	}
	catch(const Err &e)
	{
		std::cerr << "Failed with error code " << e.err() << std::endl;
	}
	catch(...)
	{
		std::cerr << "Unknown exception!" << std::endl;
	}
}


static void
GetFrameInfo(IBlackmagicRawFrame* frame, Imf::Header::AttributeMap &attrMap)
{
	try
	{
		Err err;
		
		IBlackmagicRawMetadataIterator *metadataIterator = nullptr;
		err = frame->GetMetadataIterator(&metadataIterator);
		
		do{
			CFStringRef keyRef = nullptr;
			metadataIterator->GetKey(&keyRef);
			
			const std::string key = StringRefToString(keyRef);
			
			Variant data;
			err = metadataIterator->GetData(&data);
			
			Imf::Attribute *attr = nullptr;
			
			const std::string valStr = GetVariantAttr(data, &attr);
			
			//std::cout << key << ": " << valStr << std::endl;
			
			if(attr != nullptr)
			{
				const std::string attrKey = "braw/frame/" + key;
				
				attrMap[attrKey.c_str()] = attr;
			}
			
		}while(metadataIterator->Next() == S_OK);
		
		//std::cout << std::endl;
	}
	catch(const Exc &e)
	{
		std::cerr << "Command error: " << e.what() << std::endl;
	}
	catch(const Err &e)
	{
		std::cerr << "Failed with error code " << e.err() << std::endl;
	}
	catch(...)
	{
		std::cerr << "Unknown exception!" << std::endl;
	}
}


static void
GetFrameProcessingInfo(IBlackmagicRawFrameProcessingAttributes* frameAttrs, Imf::Header::AttributeMap &attrMap)
{
	const BlackmagicRawFrameProcessingAttribute attrs[] = {
		blackmagicRawFrameProcessingAttributeWhiteBalanceKelvin,
		blackmagicRawFrameProcessingAttributeWhiteBalanceTint,
		blackmagicRawFrameProcessingAttributeExposure,
		blackmagicRawFrameProcessingAttributeISO,
		blackmagicRawFrameProcessingAttributeAnalogGain };
	
	const char * const attrNames[] = {
		"white_balance_kelvin",
		"white_balance_tint",
		"exposure",
		"iso",
		"analog_gain" };
	
	try
	{
		Err err;
		
		if(attrMap.find("braw/frame/color/white_balance_kelvin") != attrMap.end())
		{
			if(const Imf::IntAttribute *attr = dynamic_cast<const Imf::IntAttribute *>(attrMap["braw/frame/color/white_balance_kelvin"]))
			{
				SetFrameAttr<uint32_t>(frameAttrs, blackmagicRawFrameProcessingAttributeWhiteBalanceKelvin, attr->value());
			}
		}
		
		if(attrMap.find("braw/frame/color/white_balance_tint") != attrMap.end())
		{
			if(const Imf::IntAttribute *attr = dynamic_cast<const Imf::IntAttribute *>(attrMap["braw/frame/color/white_balance_tint"]))
			{
				SetFrameAttr<int16_t>(frameAttrs, blackmagicRawFrameProcessingAttributeExposure, attr->value());
			}
		}

		if(attrMap.find("braw/frame/color/exposure") != attrMap.end())
		{
			if(const Imf::FloatAttribute *attr = dynamic_cast<const Imf::FloatAttribute *>(attrMap["braw/frame/color/exposure"]))
			{
				SetFrameAttr<float>(frameAttrs, blackmagicRawFrameProcessingAttributeExposure, attr->value());
			}
		}
		
		
		const int numAttrs = (sizeof(attrs) / sizeof(BlackmagicRawClipProcessingAttribute));

		for(int i=0; i < numAttrs; i++)
		{
			const std::string key = attrNames[i];
			
			Variant data;
			HRESULT err2 = frameAttrs->GetFrameAttribute(attrs[i], &data);
			
			if(err2 == S_OK)
			{
				Imf::Attribute *attr = nullptr;
			
				const std::string valStr = GetVariantAttr(data, &attr);
			
				if(valStr != "Disabled")
				{
					//std::cout << key << ": " << valStr << std::endl;
				
					if(attr != nullptr)
					{
						const std::string attrKey = "braw/frame/color/" + key;
						
						attrMap[attrKey.c_str()] = attr;
					}
				}
			}
		}
		
		//std::cout << std::endl;
	}
	catch(const Exc &e)
	{
		std::cerr << "Command error: " << e.what() << std::endl;
	}
	catch(const Err &e)
	{
		std::cerr << "Failed with error code " << e.err() << std::endl;
	}
	catch(...)
	{
		std::cerr << "Unknown exception!" << std::endl;
	}
}

#pragma mark-

typedef struct
{
	unsigned int		hours;
	unsigned int		minutes;
	unsigned int		seconds;
	unsigned int		frame;
	bool				drop_frame;
	bool				negative;
} Time_Code;

static bool
ParseTimeCodeString(const char *str, Time_Code *timeCode)
{
	int parts[4] = {0, 0, 0, 0};
	int current_part = 0;
	int current_part_start = -1;
	bool drop_frame = false;
	bool negative = false;

	int i = 0;
	
	while(i < 16)
	{
		if(str[i] == '-')
		{
			if(i == 0)
				negative = true;
			else
				return false;
		}
		else if(str[i] >= '0' && str[i] <= '9')
		{
			if(current_part_start < 0)
				current_part_start = i;
		}
		else if(str[i] == ':' || str[i] == ';' || str[i] == '\0')
		{
			if(current_part_start >= 0 && current_part_start < i && current_part < 4)
			{
				char temp[16];
				memset(temp, 0, 16);
				
				strncpy(temp, &str[current_part_start], i - current_part_start);
				
				parts[current_part++] = atoi(temp);
				
				current_part_start = -1;
			}
			else
				return false;
			
			if(str[i] == ';')
				drop_frame = false;
			else if(str[i] == '\0')
				break;
		}
		else
			return false;
		
		i++;
	}
	
	const int total_parts = current_part;
	
	if(total_parts < 1)
		return false;
	
	assert(total_parts == 4);
	
	timeCode->frame = parts[total_parts - 1];
	
	timeCode->seconds = (total_parts >= 2 ? parts[total_parts - 2] : 0);

	timeCode->minutes = (total_parts >= 3 ? parts[total_parts - 3] : 0);

	timeCode->hours = (total_parts == 4 ? parts[total_parts - 4] : 0);
	
	assert(timeCode->frame >= 0);
	assert(timeCode->seconds >= 0 && timeCode->seconds < 60);
	assert(timeCode->minutes >= 0 && timeCode->minutes < 60);
	assert(timeCode->hours >= 0);
	
	timeCode->drop_frame = drop_frame;
	timeCode->negative = negative;

	return true;
}

#pragma mark-

class CameraCodecCallback : public IBlackmagicRawCallback
{
public:
	explicit CameraCodecCallback(const std::string &path, int bitDepth, BlackmagicRawResolutionScale scale, IBlackmagicRawClipProcessingAttributes *clipAttr, IBlackmagicRawResourceManager *resourceManager, void *commandQueue, const Imf::Header::AttributeMap &attrMap);
	virtual ~CameraCodecCallback() = default;

	virtual void ReadComplete(IBlackmagicRawJob* readJob, HRESULT result, IBlackmagicRawFrame* frame);
	virtual void ProcessComplete(IBlackmagicRawJob* job, HRESULT result, IBlackmagicRawProcessedImage* processedImage);

	virtual void DecodeComplete(IBlackmagicRawJob*, HRESULT) {}
	virtual void TrimProgress(IBlackmagicRawJob*, float) {}
	virtual void TrimComplete(IBlackmagicRawJob*, HRESULT) {}
	virtual void SidecarMetadataParseWarning(IBlackmagicRawClip*, CFStringRef, uint32_t, CFStringRef) {}
	virtual void SidecarMetadataParseError(IBlackmagicRawClip*, CFStringRef, uint32_t, CFStringRef) {}
	virtual void PreparePipelineComplete(void*, HRESULT) {}

	virtual HRESULT STDMETHODCALLTYPE QueryInterface(REFIID, LPVOID*) { return E_NOTIMPL; }
	virtual ULONG STDMETHODCALLTYPE AddRef(void) { return 0; }
	virtual ULONG STDMETHODCALLTYPE Release(void) { return 0; }
	
  private:
	const std::string _path;
	const int _bitDepth;
	BlackmagicRawResolutionScale _scale;
	IBlackmagicRawClipProcessingAttributes *_clipAttr;
	IBlackmagicRawResourceManager *_resourceManager;
	void *_commandQueue;
	const Imf::Header::AttributeMap _attrMap;
	
	typedef struct FrameInfo {
		std::string path;
		uint64_t frameIndex;
		std::string timeCode;
		Imf::Header::AttributeMap attrMap;
	} FrameInfo;

  private:
	void OutputEXR(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void* imageData);
	void OutputDPX(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void* imageData);
	void OutputPNG(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void* imageData);
	void OutputJPEG(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void* imageData);
	void OutputTIFF(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void* imageData);
};


CameraCodecCallback::CameraCodecCallback(const std::string &path, int bitDepth, BlackmagicRawResolutionScale scale, IBlackmagicRawClipProcessingAttributes *clipAttr, IBlackmagicRawResourceManager *resourceManager, void *commandQueue, const Imf::Header::AttributeMap &attrMap) :
	_path(path),
	_bitDepth(bitDepth),
	_scale(scale),
	_clipAttr(clipAttr),
	_resourceManager(resourceManager),
	_commandQueue(commandQueue),
	_attrMap(attrMap)
{
	if(_path.empty())
		throw Exc("Empty output path");
}


void
CameraCodecCallback::ReadComplete(IBlackmagicRawJob* readJob, HRESULT result, IBlackmagicRawFrame* frame)
{
	IBlackmagicRawJob* decodeAndProcessJob = nullptr;

	try
	{
		Err err = result;
		
		IBlackmagicRawFrameProcessingAttributes *frameAttr = nullptr;
		err = frame->CloneFrameProcessingAttributes(&frameAttr);
		
		BlackmagicRawResourceFormat format = blackmagicRawResourceFormatRGBF32;
		
		const std::string fileExtension = _path.substr(_path.find_last_of('.') + 1);
		
		if(fileExtension == "exr")
		{
			format = blackmagicRawResourceFormatRGBF32;
		}
		else if(fileExtension == "dpx")
		{
			format = (_bitDepth == 8 ? blackmagicRawResourceFormatRGBAU8 : blackmagicRawResourceFormatRGBU16);
		}
		else if(fileExtension == "png")
		{
			format = (_bitDepth == 16 ?  blackmagicRawResourceFormatRGBAU16 : blackmagicRawResourceFormatRGBAU8);
		}
		else if(fileExtension == "jpg" || fileExtension == "jpeg")
		{
			format = blackmagicRawResourceFormatRGBAU8;
		}
		else if(fileExtension == "tif" || fileExtension == "tiff")
		{
			format = (_bitDepth == 16 ?  blackmagicRawResourceFormatRGBAU16 : blackmagicRawResourceFormatRGBAU8);
		}
		else
			assert(false);
		
		
		Imf::Header::AttributeMap attrMap = _attrMap;
		
		GetFrameInfo(frame, attrMap);
		
		GetFrameProcessingInfo(frameAttr, attrMap);

		err = frame->SetResourceFormat(format);
		
		err = frame->SetResolutionScale(_scale);
		
		err = frame->CreateJobDecodeAndProcessFrame(_clipAttr, frameAttr, &decodeAndProcessJob);
		
		FrameInfo *frameInfo = new FrameInfo;
		
		err = frame->GetFrameIndex(&frameInfo->frameIndex);
		
		char path[1024];
		snprintf(path, 1024, _path.c_str(), frameInfo->frameIndex);
		
		frameInfo->path = path;
		
		CFStringRef timeCodeStringRef = nullptr;
		err = frame->GetTimecode(&timeCodeStringRef);
		
		if(timeCodeStringRef != nullptr)
		{
			frameInfo->timeCode = StringRefToString(timeCodeStringRef);
			
			CFRelease(timeCodeStringRef);
		}
		
		frameInfo->attrMap = attrMap;
		
		err = decodeAndProcessJob->SetUserData(frameInfo);

		err = decodeAndProcessJob->Submit();
	}
	catch(...)
	{
		if(decodeAndProcessJob != nullptr)
			decodeAndProcessJob->Release();
		
		readJob->Release();
		
		throw;
	}
	
	readJob->Release();
}


void
CameraCodecCallback::ProcessComplete(IBlackmagicRawJob *job, HRESULT result, IBlackmagicRawProcessedImage *processedImage)
{
	FrameInfo *frameInfo = nullptr;
	void *tempData = nullptr;

	try
	{
		Err err = result;
		
		uint32_t width = 0;
		uint32_t height = 0;
		uint32_t sizeBytes = 0;
		BlackmagicRawResourceFormat resourceFormat = 0;
		BlackmagicRawResourceType resourceType = 0;
		void *imageData = nullptr;
		err = processedImage->GetWidth(&width);

		err = processedImage->GetHeight(&height);

		err = processedImage->GetResourceSizeBytes(&sizeBytes);
		
		err = processedImage->GetResourceFormat(&resourceFormat);
		
		err = processedImage->GetResourceType(&resourceType);
		
		err = processedImage->GetResource(&imageData);
		
		if(resourceType != blackmagicRawResourceTypeBufferCPU)
		{
			assert(resourceType == blackmagicRawResourceTypeBufferMetal);
			
			const uint32_t bytesPerChannel = (resourceFormat == blackmagicRawResourceFormatRGBAU8 ? 1 :
											resourceFormat == blackmagicRawResourceFormatBGRAU8 ? 1 :
											resourceFormat == blackmagicRawResourceFormatRGBU16 ? 2 :
											resourceFormat == blackmagicRawResourceFormatRGBAU16 ? 2 :
											resourceFormat == blackmagicRawResourceFormatBGRAU16 ? 2 :
											resourceFormat == blackmagicRawResourceFormatRGBU16Planar ? 2 :
											resourceFormat == blackmagicRawResourceFormatRGBF32 ? 4 :
											resourceFormat == blackmagicRawResourceFormatRGBF32Planar ? 4 :
											resourceFormat == blackmagicRawResourceFormatBGRAF32 ? 4 :
											4);

			const uint32_t channelsPerPixel = (resourceFormat == blackmagicRawResourceFormatRGBAU8 ? 4 :
											resourceFormat == blackmagicRawResourceFormatBGRAU8 ? 4 :
											resourceFormat == blackmagicRawResourceFormatRGBU16 ? 3 :
											resourceFormat == blackmagicRawResourceFormatRGBAU16 ? 4 :
											resourceFormat == blackmagicRawResourceFormatBGRAU16 ? 4 :
											resourceFormat == blackmagicRawResourceFormatRGBU16Planar ? 3 :
											resourceFormat == blackmagicRawResourceFormatRGBF32 ? 3 :
											resourceFormat == blackmagicRawResourceFormatRGBF32Planar ? 3 :
											resourceFormat == blackmagicRawResourceFormatBGRAF32 ? 4 :
											4);

			const uint32_t bytesPerPixel = (bytesPerChannel * channelsPerPixel);
			
			const uint32_t tempSize = (bytesPerPixel * width * height);
			
			err = _resourceManager->CreateResource(nullptr, _commandQueue, tempSize, blackmagicRawResourceTypeBufferCPU, blackmagicRawResourceUsageReadCPUWriteCPU, &tempData);

			err = _resourceManager->CopyResource(nullptr, _commandQueue, imageData, resourceType, tempData, blackmagicRawResourceTypeBufferCPU, tempSize, false);
			
			imageData = tempData;
		}

		err = job->GetUserData((void **)&frameInfo);
		
		const std::string fileExtension = frameInfo->path.substr(frameInfo->path.find_last_of('.') + 1);
		
		if(fileExtension == "exr")
		{
			OutputEXR(frameInfo->path.c_str(), frameInfo->attrMap, frameInfo->timeCode, width, height, sizeBytes, imageData);
		}
		else if(fileExtension == "dpx")
		{
			OutputDPX(frameInfo->path.c_str(), frameInfo->attrMap, frameInfo->timeCode, width, height, sizeBytes, imageData);
		}
		else if(fileExtension == "png")
		{
			OutputPNG(frameInfo->path.c_str(), frameInfo->attrMap, frameInfo->timeCode, width, height, sizeBytes, imageData);
		}
		else if(fileExtension == "jpg" || fileExtension == "jpeg")
		{
			OutputJPEG(frameInfo->path.c_str(), frameInfo->attrMap, frameInfo->timeCode, width, height, sizeBytes, imageData);
		}
		else if(fileExtension == "tif" || fileExtension == "tiff")
		{
			OutputTIFF(frameInfo->path.c_str(), frameInfo->attrMap, frameInfo->timeCode, width, height, sizeBytes, imageData);
		}
		else
			assert(false);
	}
	catch(...)
	{
		job->Release();
		
		delete frameInfo;
		
		if(tempData)
			_resourceManager->ReleaseResource(nullptr, _commandQueue, tempData, blackmagicRawResourceTypeBufferCPU);
		
		throw;
	}
	
	job->Release();
	
	delete frameInfo;
	
	if(tempData)
		_resourceManager->ReleaseResource(nullptr, _commandQueue, tempData, blackmagicRawResourceTypeBufferCPU);
}


void
CameraCodecCallback::OutputEXR(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void *imageData)
{
	void *halfBuf = nullptr;
	
	{
		Imf::Header head(width, height);
		
		for(Imf::Header::AttributeMap::const_iterator i = attrMap.begin(); i != attrMap.end(); ++i)
		{
			head.insert(i->first.text(), *i->second);
		}
		
		Time_Code time_code;
		
		if( ParseTimeCodeString(timeCode.c_str(), &time_code) )
		{
			const Imf::RationalAttribute *fpsAttr = head.findTypedAttribute<Imf::RationalAttribute>("framesPerSecond");
			
			if(fpsAttr != nullptr)
			{
				const Imf::Rational &fps = fpsAttr->value();
				
				if(fps <= 40 && !time_code.negative)
				{
					const Imf::TimeCode imfTimeCode(time_code.hours, time_code.minutes, time_code.seconds, time_code.frame, time_code.drop_frame);
					
					Imf::addTimeCode(head, imfTimeCode);
				}
			}
			
			head.insert("timeCodeString", Imf::StringAttribute(timeCode));
		}
		
		head.insert("writer", Imf::StringAttribute("BRAWconverter"));
		
		
		const Imf::PixelType pixType = (_bitDepth == 32 ? Imf::FLOAT : Imf::HALF);
		
		head.channels().insert("R", Imf::Channel(pixType));
		head.channels().insert("G", Imf::Channel(pixType));
		head.channels().insert("B", Imf::Channel(pixType));
		
		const size_t bytesPerChannel = (pixType == Imf::HALF ? sizeof(half) : sizeof(float));
		const size_t bytesPerPixel = 3 * bytesPerChannel;
		const size_t rowBytes = width * bytesPerPixel;
		const size_t bufferSize = height * rowBytes;
		
		if(pixType == Imf::HALF)
		{
			halfBuf = malloc(bufferSize);
			
			const size_t floatRowBytes = sizeof(float) * width * 3;
			const int pixelWidth = width * 3;
			
			IlmThread::TaskGroup taskGroup;
			
			for(int y=0; y < height; y++)
			{
				IlmThread::ThreadPool::addGlobalTask(new CopyHalfTask(&taskGroup, (char *)halfBuf, rowBytes, (char *)imageData, floatRowBytes, pixelWidth, y));
			}
			
			imageData = halfBuf;
		}

		Imf::FrameBuffer frameBuffer;
	
		frameBuffer.insert("R", Imf::Slice(pixType, (char *)imageData + (bytesPerChannel * 0), bytesPerPixel, rowBytes) );
		frameBuffer.insert("G", Imf::Slice(pixType, (char *)imageData + (bytesPerChannel * 1), bytesPerPixel, rowBytes) );
		frameBuffer.insert("B", Imf::Slice(pixType, (char *)imageData + (bytesPerChannel * 2), bytesPerPixel, rowBytes) );

		Imf::OutputFile file(path, head);
		
		file.setFrameBuffer(frameBuffer);
		file.writePixels(height);
	}
	
	if(halfBuf != nullptr)
		free(halfBuf);
	
	std::cout << "Wrote " << path << std::endl;
}


void
CameraCodecCallback::OutputDPX(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void *imageData)
{
	OutStream outStream;
	
	if( !outStream.Open(path) )
		throw Exc("Failed to open file for writing");

	dpx::Writer dpx;
	
	dpx.SetOutStream(&outStream);
	
	std::string clip_number;
	
	Imf::Header::AttributeMap::const_iterator numi = attrMap.find("braw/clip/clip_number");
	
	if(numi != attrMap.end())
	{
		const Imf::StringAttribute *numattr = dynamic_cast<Imf::StringAttribute *>(numi->second);
		
		if(numattr != nullptr)
			clip_number = numattr->value();
	}
	
	dpx.SetFileInfo(clip_number.c_str(),	// fileName
					NULL,			// creation time (set by libdpx)
					"BRAWconverter", // creator
					NULL,			// project
					NULL,			// copyright
					~0,				// encryption key (0xffffffff means no encryption)
					false);			// don't swap byte order
	
	dpx.SetImageInfo(width, height);
	
	Imf::Header::AttributeMap::const_iterator fpsi = attrMap.find("framesPerSecond");
	
	if(fpsi != attrMap.end())
	{
		const Imf::RationalAttribute *fpsttr = dynamic_cast<Imf::RationalAttribute *>(fpsi->second);
		
		if(fpsttr != nullptr)
		{
			const Imf::Rational &fps = fpsttr->value();
	
			dpx.header.SetFrameRate((dpx::R32)fps.n / (dpx::R32)fps.d);
		}
	}
	
	dpx.header.SetTimeCode(timeCode.c_str());
	
	const dpx::Descriptor dpx_desc = dpx::kRGB;
	const dpx::Packing packing = ((_bitDepth == 12) ? dpx::kPacked : dpx::kFilledMethodA);
	
	const dpx::Characteristic transfer = dpx::kUnspecifiedVideo;
	const dpx::Characteristic colorimetric = dpx::kUnspecifiedVideo;
	
	dpx.SetElement(0, dpx_desc, _bitDepth, transfer, colorimetric, packing);
	
	
	const bool wrote_header = dpx.WriteHeader();
	
	if(!wrote_header)
	{
		outStream.Close();
		
		throw Exc("Error writing header");
	}
	
	
	if(_bitDepth == 8)
	{
		const size_t rowbytes = (sizeof(uint8_t) * 4 * width);
		
		IlmThread::TaskGroup taskGroup;
		
		for(int y=0; y < height; y++)
		{
			uint8_t *row = (uint8_t *)((char *)imageData + (y * rowbytes));
			
			IlmThread::ThreadPool::addGlobalTask(new RemoveAlphaTask(&taskGroup, row, width));
		}
	}
	
	const dpx::DataSize size = (_bitDepth == 8 ? dpx::kByte : dpx::kWord);
	
	const bool wrote_element = dpx.WriteElement(0, imageData, size);
	
	if(!wrote_element)
	{
		outStream.Close();
		
		throw Exc("Error writing image");
	}
	
	
	const bool wrote_finish = dpx.Finish();
	
	if(!wrote_finish)
	{
		outStream.Close();
		
		throw Exc("Error writing finish");
	}
	
	outStream.Close();
	
	std::cout << "Wrote " << path << std::endl;
}


void
CameraCodecCallback::OutputPNG(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void *imageData)
{
	size_t bytesPerRow = (_bitDepth * 4 * width) / 8;
	CGColorSpaceRef space = CGColorSpaceCreateWithName(kCGColorSpaceSRGB);
	CGBitmapInfo bitmapInfo = (_bitDepth == 16 ? kCGImageByteOrder16Little : kCGImageByteOrderDefault);
	CGDataProviderRef provider = CGDataProviderCreateWithData(nullptr, imageData, sizeBytes, nullptr);
	const CGFloat* decode = nullptr;
	bool shouldInterpolate = false;
	CGColorRenderingIntent intent = kCGRenderingIntentDefault;

	CGImageRef imageRef = CGImageCreate(width, height, _bitDepth, _bitDepth * 4, bytesPerRow, space, bitmapInfo, provider, decode, shouldInterpolate, intent);
	
	CFStringRef pathRef = CFStringCreateWithCString(kCFAllocatorDefault, path, kCFStringEncodingUTF8);
	
	CFURLRef file = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, pathRef, kCFURLPOSIXPathStyle, false);
	
	if(file == nullptr)
		throw Exc("Failed to create file for writing");

	CGImageDestinationRef destination = CGImageDestinationCreateWithURL(file, kUTTypePNG, 1, nullptr);
	
	if(destination)
	{
		CGImageDestinationAddImage(destination, imageRef, nullptr);
		CGImageDestinationFinalize(destination);
		
		CFRelease(destination);
	}

	CFRelease(file);
	CFRelease(pathRef);
	CGImageRelease(imageRef);
	CGDataProviderRelease(provider);
	CGColorSpaceRelease(space);
	
	std::cout << "Wrote " << path << std::endl;
}


void
CameraCodecCallback::OutputJPEG(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void *imageData)
{
	size_t bytesPerRow = (_bitDepth * 4 * width) / 8;
	CGColorSpaceRef space = CGColorSpaceCreateWithName(kCGColorSpaceSRGB);
	CGBitmapInfo bitmapInfo = (_bitDepth == 16 ? kCGImageByteOrder16Little : kCGImageByteOrderDefault);
	CGDataProviderRef provider = CGDataProviderCreateWithData(nullptr, imageData, sizeBytes, nullptr);
	const CGFloat* decode = nullptr;
	bool shouldInterpolate = false;
	CGColorRenderingIntent intent = kCGRenderingIntentDefault;

	CGImageRef imageRef = CGImageCreate(width, height, _bitDepth, _bitDepth * 4, bytesPerRow, space, bitmapInfo, provider, decode, shouldInterpolate, intent);
	
	CFStringRef pathRef = CFStringCreateWithCString(kCFAllocatorDefault, path, kCFStringEncodingUTF8);
	
	CFURLRef file = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, pathRef, kCFURLPOSIXPathStyle, false);
	
	if(file == nullptr)
		throw Exc("Failed to create file for writing");

	CGImageDestinationRef destination = CGImageDestinationCreateWithURL(file, kUTTypeJPEG, 1, nullptr);
	
	if(destination)
	{
		CFDictionaryRef jpegDict = nullptr;
		
		Imf::Header::AttributeMap::const_iterator i = attrMap.find("jpegCompressionLevel");
		
		if(i != attrMap.end())
		{
			if(const Imf::FloatAttribute *compression = dynamic_cast<const Imf::FloatAttribute *>(i->second))
			{
				const float compresison = compression->value();
				
				CFStringRef jpegKey = kCGImageDestinationLossyCompressionQuality;
				CFNumberRef jpegVal = CFNumberCreate(kCFAllocatorDefault, kCFNumberFloatType, &compresison);
				jpegDict = CFDictionaryCreate(kCFAllocatorDefault, (const void **)&jpegKey, (const void **)&jpegVal, 1, nullptr, nullptr);
			}
		}
	
		CGImageDestinationAddImage(destination, imageRef, jpegDict);
		CGImageDestinationFinalize(destination);
		
		if(jpegDict)
			CFRelease(jpegDict);
		
		CFRelease(destination);
	}

	CFRelease(file);
	CFRelease(pathRef);
	CGImageRelease(imageRef);
	CGDataProviderRelease(provider);
	CGColorSpaceRelease(space);
	
	std::cout << "Wrote " << path << std::endl;
}


void
CameraCodecCallback::OutputTIFF(const char *path, const Imf::Header::AttributeMap &attrMap, const std::string &timeCode, unsigned int width, unsigned int height, size_t sizeBytes, void *imageData)
{
	size_t bytesPerRow = (_bitDepth * 4 * width) / 8;
	CGColorSpaceRef space = CGColorSpaceCreateWithName(kCGColorSpaceSRGB);
	CGBitmapInfo bitmapInfo = (_bitDepth == 16 ? kCGImageByteOrder16Little : kCGImageByteOrderDefault);
	CGDataProviderRef provider = CGDataProviderCreateWithData(nullptr, imageData, sizeBytes, nullptr);
	const CGFloat* decode = nullptr;
	bool shouldInterpolate = false;
	CGColorRenderingIntent intent = kCGRenderingIntentDefault;

	CGImageRef imageRef = CGImageCreate(width, height, _bitDepth, _bitDepth * 4, bytesPerRow, space, bitmapInfo, provider, decode, shouldInterpolate, intent);
	
	CFStringRef pathRef = CFStringCreateWithCString(kCFAllocatorDefault, path, kCFStringEncodingUTF8);
	
	CFURLRef file = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, pathRef, kCFURLPOSIXPathStyle, false);
	
	if(file == nullptr)
		throw Exc("Failed to create file for writing");

	CGImageDestinationRef destination = CGImageDestinationCreateWithURL(file, kUTTypeTIFF, 1, nullptr);
	
	if(destination)
	{
		CGImageDestinationAddImage(destination, imageRef, nullptr);
		CGImageDestinationFinalize(destination);
		
		CFRelease(destination);
	}

	CFRelease(file);
	CFRelease(pathRef);
	CGImageRelease(imageRef);
	CGDataProviderRelease(provider);
	CGColorSpaceRelease(space);
	
	std::cout << "Wrote " << path << std::endl;
}

#pragma mark-

static void
ProcessArguments(const std::vector<std::string> &args, const std::string &outputPath, uint64_t &start, uint64_t &end, int &bitDepth, BlackmagicRawResolutionScale &scale, IBlackmagicRawClipProcessingAttributes *clipAttrs, Imf::Header::AttributeMap &attrMap, IBlackmagicRawConstants *constants, CFStringRef cameraType)
{
	Err err;
	
	const std::string::size_type lastDot = outputPath.find_last_of('.');

	if(lastDot == std::string::npos || lastDot == (outputPath.size() - 1))
		throw Exc("No file extension on output path");

	const std::string fileExtension = outputPath.substr(lastDot + 1);
	
	if(fileExtension == "exr")
	{
		Variant gamma;
		gamma.vt = blackmagicRawVariantTypeString;
		gamma.bstrVal = CFSTR("Linear");
		err = clipAttrs->SetClipAttribute(blackmagicRawClipProcessingAttributeGamma, &gamma);
		
		bitDepth = 16;
		
		attrMap["compression"] = new Imf::CompressionAttribute(Imf::PIZ_COMPRESSION);
	}
	else if(fileExtension == "dpx")
	{
		bitDepth = 10;
	}
	else if(fileExtension == "png")
	{
		bitDepth = 8;
	}
	else if(fileExtension == "jpg" || fileExtension == "jpeg")
	{
		bitDepth = 8;
	}
	else if(fileExtension == "tif" || fileExtension == "tiff")
	{
		bitDepth = 8;
	}
	else
		throw Exc("Invalid output file extension.  Must be .exr, .png, .jpg");

	
	for(int i = 1; i < (args.size() - 1); i += 2) // all args come in pairs
	{
		const std::string &arg = args[i];
		const std::string &nextArg = args[i + 1];
		
		if(arg == "-o")
		{
			assert(outputPath == nextArg);
		}
		else if(arg == "-s")
		{
			const int newStart = std::stoi(nextArg);
			
			if(start <= newStart)
				start = newStart;
			else
				throw Exc("Invalid start frame");
		}
		else if(arg == "-e")
		{
			const int newEnd = std::stoi(nextArg);
			
			if(end >= newEnd)
				end = newEnd;
			else
				throw Exc("Invalid end frame");
		}
		else if(arg == "--pipeline")
		{
			// pipeline
		}
		else if(arg == "--scale")
		{
			if(nextArg == "full")
				scale = blackmagicRawResolutionScaleFull;
			else if(nextArg == "half")
				scale = blackmagicRawResolutionScaleHalf;
			else if(nextArg == "quarter")
				scale = blackmagicRawResolutionScaleQuarter;
			else if(nextArg == "eighth")
				scale = blackmagicRawResolutionScaleEighth;
			else
				throw Exc("Scale must be full, half, quarter, or eighth");
		}
		else if(arg == "--bitDepth")
		{
			bitDepth = std::stoi(nextArg);
		}
		else if(arg == "--exrCompression")
		{
			Imf::Compression compression = Imf::PIZ_COMPRESSION;
		
			if(nextArg == "none")
				compression = Imf::NO_COMPRESSION;
			else if(nextArg == "rle")
				compression = Imf::RLE_COMPRESSION;
			else if(nextArg == "zips")
				compression = Imf::ZIPS_COMPRESSION;
			else if(nextArg == "zip")
				compression = Imf::ZIP_COMPRESSION;
			else if(nextArg == "piz")
				compression = Imf::PIZ_COMPRESSION;
			else if(nextArg == "pxr24")
				compression = Imf::PXR24_COMPRESSION;
			else if(nextArg == "b44")
				compression = Imf::B44_COMPRESSION;
			else if(nextArg == "b44a")
				compression = Imf::B44A_COMPRESSION;
			else if(nextArg == "dwaa")
				compression = Imf::DWAA_COMPRESSION;
			else if(nextArg == "dwab")
				compression = Imf::DWAB_COMPRESSION;
			else
				throw Exc("Invalid EXR compression option, see --help");

			attrMap["compression"] = new Imf::CompressionAttribute(compression);
			
			
			if(compression == Imf::DWAA_COMPRESSION || compression == Imf::DWAB_COMPRESSION)
			{
				if(attrMap.find("dwaCompressionLevel") == attrMap.end())
				{
					attrMap["dwaCompressionLevel"] = new Imf::FloatAttribute(45.0);
				}
			}
		}
		else if(arg == "--dwaCompressionLevel")
		{
			attrMap["dwaCompressionLevel"] = new Imf::FloatAttribute(std::stof(nextArg));
		}
		else if(arg == "--jpegCompressionLevel")
		{
			const float compression = std::stof(nextArg);
			
			if(compression >= 0.f && compression <= 1.f)
				attrMap["jpegCompressionLevel"] = new Imf::FloatAttribute(std::stof(nextArg));
			else
				throw Exc("JPEG compression must be between 0.0 and 1.0");
		}
		else if(arg == "--gamma")
		{
			SetClipAttr<CFStringRef>(clipAttrs, blackmagicRawClipProcessingAttributeGamma, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--gamut")
		{
			SetClipAttr<CFStringRef>(clipAttrs, blackmagicRawClipProcessingAttributeGamut, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--contrast")
		{
			SetClipAttr<float>(clipAttrs, blackmagicRawClipProcessingAttributeToneCurveContrast, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--saturation")
		{
			SetClipAttr<float>(clipAttrs, blackmagicRawClipProcessingAttributeToneCurveSaturation, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--midpoint")
		{
			SetClipAttr<float>(clipAttrs, blackmagicRawClipProcessingAttributeToneCurveMidpoint, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--highlights")
		{
			SetClipAttr<float>(clipAttrs, blackmagicRawClipProcessingAttributeToneCurveHighlights, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--shadows")
		{
			SetClipAttr<float>(clipAttrs, blackmagicRawClipProcessingAttributeToneCurveShadows, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--video_black_level")
		{
			SetClipAttr<uint16_t>(clipAttrs, blackmagicRawClipProcessingAttributeToneCurveVideoBlackLevel, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--black_level")
		{
			SetClipAttr<float>(clipAttrs, blackmagicRawClipProcessingAttributeToneCurveBlackLevel, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--white_level")
		{
			SetClipAttr<float>(clipAttrs, blackmagicRawClipProcessingAttributeToneCurveWhiteLevel, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--highlight_recovery")
		{
			SetClipAttr<uint16_t>(clipAttrs, blackmagicRawClipProcessingAttributeHighlightRecovery, arg, nextArg, constants, cameraType);
		}
		else if(arg == "--white_balance_kelvin")
		{
			CheckFrameAttr<uint32_t>(blackmagicRawFrameProcessingAttributeWhiteBalanceKelvin, arg, nextArg, constants, cameraType);
			
			attrMap["braw/frame/color/white_balance_kelvin"] = new Imf::IntAttribute(std::stoi(nextArg));
		}
		else if(arg == "--white_balance_tint")
		{
			CheckFrameAttr<int16_t>(blackmagicRawFrameProcessingAttributeWhiteBalanceTint, arg, nextArg, constants, cameraType);
			
			attrMap["braw/frame/color/white_balance_tint"] = new Imf::IntAttribute(std::stoi(nextArg));
		}
		else if(arg == "--exposure")
		{
			CheckFrameAttr<float>(blackmagicRawFrameProcessingAttributeExposure, arg, nextArg, constants, cameraType);
			
			attrMap["braw/frame/color/exposure"] = new Imf::FloatAttribute(std::stof(nextArg));
		}
		else
			throw Exc("Invalid argument: " + arg);
	}
	
	if(end < start)
		throw Exc("End can't be before start");
	
	if(fileExtension == "exr")
	{
		if(bitDepth != 16 && bitDepth != 32)
			throw Exc("OpenEXR bit depth must be 16 or 32");
	}
	if(fileExtension == "dpx")
	{
		if(bitDepth != 8 && bitDepth != 10 && bitDepth != 12 && bitDepth != 16)
			throw Exc("DPX bit depth must be 8, 10, 12, 16");
	}
	else if(fileExtension == "png")
	{
		if(bitDepth != 8 && bitDepth != 16)
			throw Exc("PNG bit depth must be 8 or 16");
	}
	else if(fileExtension == "jpg" || fileExtension == "jpeg")
	{
		if(bitDepth != 8)
			throw Exc("JPEG bit depth must be 8");
	}
	else if(fileExtension == "tif" || fileExtension == "tiff")
	{
		if(bitDepth != 8 && bitDepth != 16)
			throw Exc("TIFF bit depth must be 8 or 16");
	}
}


static void
PrintHelp()
{
	static const char *help =
"" "\n"
"Usage: BRAWconverter [args] [-o outputSequence.%04d.exr] clipName.braw" "\n"
"" "\n"
"-o outputSequence.%04d.exr" "\n"
"        Image sequence to output.  If missing, BRAW file info will be printed" "\n"
"        and exit.  Supported file formats are OpenEXR, DPX, PNG, JPEG, and" "\n"
"        TIFF.  You will usually want to use a formatted string token" "\n"
"        (like %04d) to place the frame number." "\n"
"" "\n"
"-s frame" "\n"
"        Frame index in the BRAW to start rendering on.  Use 0 for the first" "\n"
"        frame (the default), [number of frames] - 1 for the last frame" "\n"
"        in the movie." "\n"
"" "\n"
"-e frame" "\n"
"        Last frame index to render.  Defaults to the last frame." "\n"
"" "\n"
"--scale amt" "\n"
"        Scale down the output." "\n"
"        Set to one of: full, half, quarter, eighth." "\n"
"" "\n"
"--bitDepth depth" "\n"
"        Bit depth to use for output file.  Supported depths depends on the" "\n"
"        output format." "\n"
"            EXR: 16*, 32" "\n"
"            DPX: 8, 10*, 12, 16" "\n"
"            PNG: 8*, 16" "\n"
"            JPEG: 8*" "\n"
"            TIFF: 8*, 16" "\n"
"        * = default" "\n"
"" "\n"
"--pipeline" "\n"
"        CPU/GPU pipeline" "\n"
"        Options are: cpu, metal" "\n"
"" "\n"
"--exrCompression comp" "\n"
"        Compression to use in an OpenEXR file.  Piz is the default." "\n"
"        Options are: none, rle, zips, zip, piz, pxr24, b44, b44a, dwaa, dwab" "\n"
"" "\n"
"--dwaCompressionLevel lvl" "\n"
"        If using DWAA or DWAB compression with OpenEXR files, sets the" "\n"
"        quantization.  Use a floating point number.  The default is 45.0." "\n"
"        Higher values means more compression, i.e. lower image quality" "\n"
"        but smaller files." "\n"
"" "\n"
"--jpegCompressionLevel lvl" "\n"
"        Set the compression for JPEG files.  Use a floating point number" "\n"
"        between 0.0 and 1.0.  0.0 is super lossy, 1.0 is near lossless." "\n"
"" "\n"
"" "\n"
"Processing Options" "\n"
"" "\n"
"--gamma string" "\n"
"        Set the gamma curve used in the conversion.  The default is to use" "\n"
"        whatever is in the footage, except in the case of OpenEXR where the" "\n"
"        default is Linear.  The options are (use quotes in your shell" "\n"
"        as necessary):" "\n"
"            \"Blackmagic Design Film\"" "\n"
"            \"Blackmagic Design Video\"" "\n"
"            \"Blackmagic Design Extended Video\"" "\n"
"            \"Blackmagic Design Custom\"" "\n"
"            \"Linear\"" "\n"
"            \"Rec.2100 Hybrid Log Gamma\"" "\n"
"            \"Rec.2100 ST2084 (PQ)\"" "\n"
"            \"Rec.709\"" "\n"
"            \"ACEScct\"" "\n"
"            \"V-Log\"" "\n"
"            \"Canon Log2\"" "\n"
"" "\n"
"--gamut string" "\n"
"        Set the color space gamut to be used in the conversion.  If you" "\n"
"        set a wide gamut, the result will appear desaturated when viewed" "\n"
"        without any color management." "\n"
"        The options are (use quotes in your shell as necessary):" "\n"
"            \"Blackmagic Design\"" "\n"
"            \"Rec.709\"" "\n"
"            \"Rec.2020\"" "\n"
"            \"DCI-P3 D65\"" "\n"
"            \"DCI-P3 Theater\"" "\n"
"            \"ACES AP0\"" "\n"
"            \"ACES AP1\"" "\n"
"            \"CIE 1931 XYZ D65\"" "\n"
"            \"CIE 1931 XYZ D50 (PCS)\"" "\n"
"            \"V-Gamut\"" "\n"
"            \"Canon Cinema Gamut\"" "\n"
"" "\n"
"Gamma options, hopefully self-explanatory (or see the BRAW SDK)." "\n"
"These only apply when gamma is Blackmagic Design Film," "\n"
"Blackmagic Design Extended Video, or Blackmagic Design Custom." "\n"
"" "\n"
"--contrast float" "\n"
"--saturation float" "\n"
"--midpoint float" "\n"
"--highlights float" "\n"
"--shadows float" "\n"
"--video_black_level bool" "\n"
"--black_level float" "\n"
"--white_level float" "\n"
"--highlight_recovery bool" "\n"
"" "\n"
"Color options" "\n"
"" "\n"
"--white_balance_kelvin int" "\n"
"--white_balance_tint int" "\n"
"--exposure float" "\n"
"" "\n";

	std::cout << help;
}

#pragma mark-

int
main(int argc, const char* argv[])
{
	std::string inputPath;
	std::string outputPath;
	BlackmagicRawPipeline pipeline = blackmagicRawPipelineCPU;

	
	if(argc < 2)
	{
		std::cerr << "Usage: BRAWconverter [args] [-o outputSequence.%04d.exr] clipName.braw" << std::endl;
		std::cerr << "BRAWconverter --help for instructions" << std::endl;
		return 1;
	}
	
	
	std::cout << "BRAWconverter by Brendan Bolles, version 1.0, " << __DATE__ << std::endl;
	
	
	std::vector<std::string> args;
	
	for(int i=0; i < argc; i++)
		args.push_back(argv[i]);
	
	
	try
	{
		for(int i=1; i < args.size(); i++)
		{
			const std::string &arg = args[i];
			const std::string &nextArg = (i < (args.size() - 1) ? args[i + 1] : std::string(""));
			
			if(arg == "--help")
			{
				PrintHelp();
				return 0;
			}
			else if(arg == "-o")
			{
				if(!nextArg.empty())
				{
					outputPath = nextArg;
					i++;
				}
				else
					throw Exc("Bad output file argument");
			}
			else if(arg == "--pipeline")
			{
				if(nextArg == "cpu")
					pipeline = blackmagicRawPipelineCPU;
				else if(nextArg == "metal")
					pipeline = blackmagicRawPipelineMetal;
				else
					throw Exc("Pipeline must be cpu or metal");
				
				i++;
			}
			else if(i == (args.size() - 1))
			{
				inputPath = arg;
			}
		}
		
		if(inputPath.empty())
			throw Exc("No input path provided");
	}
	catch(const Exc &e)
	{
		std::cerr << "Command error: " << e.what() << std::endl;
		std::cerr << "BRAWconverter --help for instructions" << std::endl;
		return E_FAIL;
	}
	catch(...)
	{
		std::cerr << "Unknown exception!" << std::endl;
		std::cerr << "BRAWconverter --help for instructions" << std::endl;
		return E_FAIL;
	}
	
	
	if( IlmThread::supportsThreads() )
	{
		host_basic_info_data_t hostInfo;
		mach_msg_type_number_t infoCount;
		
		infoCount = HOST_BASIC_INFO_COUNT;
		host_info(mach_host_self(), HOST_BASIC_INFO,
				  (host_info_t)&hostInfo, &infoCount);

		Imf::setGlobalThreadCount(hostInfo.max_cpus);
	}
	
	
	int result = S_OK;
	
	int bitDepth = 16;
	BlackmagicRawResolutionScale scale = blackmagicRawResolutionScaleFull;
	
	IBlackmagicRawFactory* factory = nullptr;
	IBlackmagicRaw* codec = nullptr;
	IBlackmagicRawClip* clip = nullptr;
	IBlackmagicRawResourceManager *resourceManager = nullptr;
	
	void *metalCommandQueue = nullptr;

	try
	{
		factory = CreateBlackmagicRawFactoryInstance();
		
		if(factory == nullptr)
			throw Exc("Failed to load BlackmagicRawAPI.framework");
		
		
		CFBundleRef bundleRef = CFBundleGetBundleWithIdentifier(CFSTR("com.blackmagic-design.blackmagicraw.api"));

		if(bundleRef != nullptr)
		{
			CFDictionaryRef infoRef = CFBundleGetInfoDictionary(bundleRef);
			
			CFStringRef val = nullptr;
			
			if( CFDictionaryGetValueIfPresent(infoRef, CFSTR("CFBundleShortVersionString"), (const void **)&val) )
			{
				std::cout << "BlackmagicRawAPI version " << StringRefToString(val) << std::endl;
			}
		}
		
		std::cout << std::endl;
		

		Err err;
		
		err = factory->CreateCodec(&codec);
		
		if(pipeline == blackmagicRawPipelineMetal)
		{
			GetMetalCommandQueue(&metalCommandQueue);
			
			if(metalCommandQueue != nullptr)
			{
				IBlackmagicRawConfiguration *config = nullptr;
				err = codec->QueryInterface(IID_IBlackmagicRawConfiguration, (void **)&config);
				
				HRESULT err2 = config->SetPipeline(blackmagicRawPipelineMetal, nullptr, metalCommandQueue);
				
				if(err2 == S_OK)
				{
					std::cout << "Pipeline is Metal" << std::endl << std::endl;
					
					IBlackmagicRawConfigurationEx *configEx = nullptr;
					err = codec->QueryInterface(IID_IBlackmagicRawConfigurationEx, (void **)&configEx);
					
					err = configEx->GetResourceManager(&resourceManager);
				}
				else
					throw Exc("Failed to set up Metal pipeline");
			}
			else
				throw Exc("Failed to initialize Metal");
		}
		
		CFStringRef inputPathRef = CFStringCreateWithCString(kCFAllocatorDefault, inputPath.c_str(), kCFStringEncodingUTF8);
		
		HRESULT clipErr = codec->OpenClip(inputPathRef, &clip);
		
		if(clipErr != S_OK)
			throw Exc("Failed to open " + inputPath);
		
		Imf::Header::AttributeMap attrMap;
		
		GetClipInfo(clip, attrMap);
		
		IBlackmagicRawClipProcessingAttributes *clipAttrs = nullptr;
		err = clip->CloneClipProcessingAttributes(&clipAttrs);
		
		if(!outputPath.empty())
		{
			uint64_t frameCount = 0;
			err = clip->GetFrameCount(&frameCount);
			
			uint64_t start = 0;
			uint64_t end = (frameCount - 1);
			
			IBlackmagicRawConstants *constants = nullptr;
			err = codec->QueryInterface(IID_IBlackmagicRawConstants, (void **)&constants); // obviously
			
			CFStringRef cameraType = nullptr;
			err = clip->GetCameraType(&cameraType);
			
			ProcessArguments(args, outputPath, start, end, bitDepth, scale, clipAttrs, attrMap, constants, cameraType);
			
			GetClipProcessingInfo(clipAttrs, attrMap);
			
			CameraCodecCallback callback(outputPath, bitDepth, scale, clipAttrs, resourceManager, metalCommandQueue, attrMap);
			
			err = codec->SetCallback(&callback);
			
			for(uint64_t frame = start; frame <= end; frame++)
			{
				IBlackmagicRawJob* readJob = nullptr;
				err = clip->CreateJobReadFrame(frame, &readJob);
				
				HRESULT err2 = readJob->Submit();
				
				if(err2 != S_OK)
				{
					readJob->Release();
					
					err = err2;
				}
			}
			
			err = codec->FlushJobs();
		}
		else
			GetClipProcessingInfo(clipAttrs, attrMap);

	}
	catch(const Exc &e)
	{
		std::cerr << "Exception: " << e.what() << std::endl;
		result = E_FAIL;
	}
	catch(const Err &e)
	{
		std::cerr << "Failed with error code " << e.err() << std::endl;
		result = e.err();
	}
	catch(...)
	{
		std::cerr << "Unknown exception!" << std::endl;
		result = E_FAIL;
	}
	
	if( IlmThread::supportsThreads() )
		Imf::setGlobalThreadCount(0);

	if(metalCommandQueue != nullptr)
		ReleaseMetalCommandQueue(metalCommandQueue);

	if(clip != nullptr)
		clip->Release();
	
	if(codec != nullptr)
		codec->Release();

	if(factory != nullptr)
		factory->Release();
	
	return result;
}
