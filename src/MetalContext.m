
#include "MetalContext.h"

#import <Metal/Metal.h>

void GetMetalCommandQueue(void **commandQueue)
{
	NSArray<id<MTLDevice>> *allDevices = MTLCopyAllDevices();
	
	if([allDevices count] > 0)
	{
		for(NSUInteger i=0; i < [allDevices count]; i++)
		{
			id<MTLDevice> device = [allDevices objectAtIndex:i];
			
			if([device supportsFeatureSet:MTLFeatureSet_OSX_GPUFamily1_v2])
			{
				id<MTLCommandQueue> commandQueueId = [device newCommandQueue];
				
				*commandQueue = (__bridge void*)commandQueueId;
				
				break;
			}
		}
	}
	
	[allDevices release];
}


void ReleaseMetalCommandQueue(void *commandQueue)
{
	id<MTLCommandQueue> mtlCommandQueue = (__bridge id<MTLCommandQueue>)(commandQueue);
	
	[mtlCommandQueue release];
}
