

#ifndef METAL_CONTEXT_H
#define METAL_CONTEXT_H

#ifdef __cplusplus
	extern "C" {
#endif

void GetMetalCommandQueue(void **commandQueue);
void ReleaseMetalCommandQueue(void *commandQueue);

#ifdef __cplusplus
	}
#endif

#endif // METAL_CONTEXT_H
